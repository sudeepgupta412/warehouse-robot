#
# === Introduction ===
#
# In this problem, you will build a planner that helps a robot
#   find the best policy through a warehouse filled with boxes
#   that it has to pick up and deliver to a dropzone.
#
# Your file must be called `partA.py` and must have a class
#   called `DeliveryPlanner`.
# This class must have an `__init__` function that takes three
#   arguments: `self`, `warehouse`, and `todo`.
# The class must also have a function called `plan_delivery` that
#   takes a single argument, `self`.
#
# === Input Specifications ===
#
# `warehouse` will be a list of m strings, each with n characters,
#   corresponding to the layout of the warehouse. The warehouse is an
#   m x n grid. warehouse[i][j] corresponds to the spot in the ith row
#   and jth column of the warehouse, where the 0th row is the northern
#   end of the warehouse and the 0th column is the western end.
#
# The characters in each string will be one of the following:
#
# '.' (period) : traversable space. The robot may enter from any adjacent space.
# '#' (hash) : a wall. The robot cannot enter this space.
# '@' (dropzone): the starting point for the robot and the space where all boxes must be delivered.
#   The dropzone may be traversed like a '.' space.
# [0-9a-zA-Z] (any alphanumeric character) : a box. At most one of each alphanumeric character
#   will be present in the warehouse (meaning there will be at most 62 boxes). A box may not
#   be traversed, but if the robot is adjacent to the box, the robot can pick up the box.
#   Once the box has been removed, the space functions as a '.' space.
#
# For example,
#   warehouse = ['1#2',
#                '.#.',
#                '..@']
#   is a 3x3 warehouse.
#   - The dropzone is at the warehouse cell in row 2, column 2.
#   - Box '1' is located in the warehouse cell in row 0, column 0.
#   - Box '2' is located in the warehouse cell in row 0, column 2.
#   - There are walls in the warehouse cells in row 0, column 1 and row 1, column 1.
#   - The remaining five warehouse cells contain empty space.
#
# The argument `todo` is a list of alphanumeric characters giving the order in which the
#   boxes must be delivered to the dropzone. For example, if
#   todo = ['1','2']
#   is given with the above example `warehouse`, then the robot must first deliver box '1'
#   to the dropzone, and then the robot must deliver box '2' to the dropzone.
#
# === Rules for Movement ===
#
# - Two spaces are considered adjacent if they share an edge or a corner.
# - The robot may move horizontally or vertically at a cost of 2 per move.
# - The robot may move diagonally at a cost of 3 per move.
# - The robot may not move outside the warehouse.
# - The warehouse does not "wrap" around.
# - As described earlier, the robot may pick up a box that is in an adjacent square.
# - The cost to pick up a box is 4, regardless of the direction the box is relative to the robot.
# - While holding a box, the robot may not pick up another box.
# - The robot may put a box down on an adjacent empty space ('.') or the dropzone ('@') at a cost
#   of 2 (regardless of the direction in which the robot puts down the box).
# - If a box is placed on the '@' space, it is considered delivered and is removed from the ware-
#   house.
# - The warehouse will be arranged so that it is always possible for the robot to move to the
#   next box on the todo list without having to rearrange any other boxes.
#
# An illegal move will incur a cost of 100, and the robot will not move (the standard costs for a
#   move will not be additionally incurred). Illegal moves include:
# - attempting to move to a nonadjacent, nonexistent, or occupied space
# - attempting to pick up a nonadjacent or nonexistent box
# - attempting to pick up a box while holding one already
# - attempting to put down a box on a nonadjacent, nonexistent, or occupied space
# - attempting to put down a box while not holding one
#
# === Output Specifications ===
#
# `plan_delivery` should return a LIST of moves that minimizes the total cost of completing
#   the task successfully.
# Each move should be a string formatted as follows:
#
# 'move {i} {j}', where '{i}' is replaced by the row-coordinate of the space the robot moves
#   to and '{j}' is replaced by the column-coordinate of the space the robot moves to
#
# 'lift {x}', where '{x}' is replaced by the alphanumeric character of the box being picked up
#
# 'down {i} {j}', where '{i}' is replaced by the row-coordinate of the space the robot puts
#   the box, and '{j}' is replaced by the column-coordinate of the space the robot puts the box
#
# For example, for the values of `warehouse` and `todo` given previously (reproduced below):
#   warehouse = ['1#2',
#                '.#.',
#                '..@']
#   todo = ['1','2']
# `plan_delivery` might return the following:
#   ['move 2 1',
#    'move 1 0',
#    'lift 1',
#    'move 2 1',
#    'down 2 2',
#    'move 1 2',
#    'lift 2',
#    'down 2 2']
#
# === Grading ===
#
# - Your planner will be graded against a set of test cases, each equally weighted.
# - If your planner returns a list of moves of total cost that is K times the minimum cost of
#   successfully completing the task, you will receive 1/K of the credit for that test case.
# - Otherwise, you will receive no credit for that test case. This could happen for one of several
#   reasons including (but not necessarily limited to):
#   - plan_delivery's moves do not deliver the boxes in the correct order.
#   - plan_delivery's output is not a list of strings in the prescribed format.
#   - plan_delivery does not return an output within the prescribed time limit.
#   - Your code raises an exception.
#
# === Additional Info ===
#
# - You may add additional classes and functions as needed provided they are all in the file `partA.py`.
# - Upload partA.py to Project 2 on T-Square in the Assignments section. Do not put it into an
#   archive with other files.
# - Your partA.py file must not execute any code when imported.
# - Ask any questions about the directions or specifications on Piazza.
#

class DeliveryPlanner:
    def __init__(self, warehouse, todo):
        self.warehouse = warehouse
        self.todo = todo
        self.rows = len(warehouse)
        self.cols = len(warehouse[0])
        #self.dropzone = (0,0)
        self.dropzone = 0, 0
        self.cost_horizontal = 2
        self.cost_diagonal = 3
        self.cost_vertical = 2
        self.classic_todo = self.classic_todo_func(todo, warehouse)
        
        self.heuristic = self.define_heuristic(self.classic_todo, self.dropzone)
        self.moves_costs =  [2, 2, 2, 2, 3, 3, 3, 3]
        self.delta_name = ['u', 'l','d','r','u-r','d-r','d-l','u-l']
        self.delta = [[-1, 0],  # go up
                      [0, -1],  # go left
                      [1, 0],  # go down
                      [0, 1],  # do right
                      [1, 1],  # top right
                      [1, -1],  # bottom right
                      [-1, 1],  # bottom left
                      [-1, -1]]  # top left

        
 
    def policy_string(self,policy,directions,moves,start_point, end_point, packet, target, curr_packet,delta_name):
        x0 = start_point[0]
        y0 = start_point[1]
        self.previous = start_point
        #self.coordinates = []
        #print 'packet status = ' + str(packet) + ' ' + str(end_point)
        x,y = x0,y0
        i = 0
        while ((x == end_point[0]) and (y == end_point[1])) != 1:
            #print y
            i = i + 1
            for delta in range(len(delta_name)):
                if policy[x][y] == delta_name[delta]:
                    string = ''
                    x = x + directions[delta][0]
                    y = y + directions[delta][1]
                    
                    #print 'packet =' + str(packet) + ' coordinates = ' + str((x,y))
                    if (packet == 0): 
                        if (x == end_point[0] and y == end_point[1]):
                            string = 'lift ' + str(curr_packet)
                            
                        else:
                            string = 'move ' + str(x) + ' ' + str(y)
                            self.previous = (x,y)
                    elif (packet == 1) :
                        if (x == end_point[0] and y == end_point[1]):
                            string = 'down ' + str(x) + ' ' + str(y)
                        else:
                            string = 'move ' + str(x) + ' ' + str(y)
                            self.previous = (x,y)
                    #print moves
                    if packet == 1 and i == 1:
                        #print 'I am at this locaotin' + str(x) + ',' + str(y)
                        break
                    else:
                        moves.append(string)
                        
                        break
                    
        
#        if (packet == 0) :
#            string = 'lift ' + str(curr_packet)
#            moves.append(string)
#        if (packet == 1) :
#            string = 'down ' + str(end_point[0]) + ' ' + str(end_point[1])  
#            moves.append(string)
        #print self.coordinates    
        return moves
    
    
    def plan_delivery(self):
        grid = self.warehouse
        string = ''
        for i in range(self.rows):
            for j in range(self.cols):
                if grid[i][j] == '@':
                    self.dropzone = i, j
        heuristic = self.heuristic
        robot_start = self.dropzone

        moves = []
        #item = self.classic_todo.pop(0)
        #print item
        for ind in range(len(self.classic_todo)):
            item = self.classic_todo[ind]
            #print item
            
            if not self.adjacent_cell(item, robot_start):
            
                
                policy, robot_start = self.astar(heuristic, robot_start, grid, item)
                # print "pick_policy", policy
                for j in range(len(policy)):
                    string = "move " + str(policy[j][0]) + " " + str(policy[j][1])
                    moves.append(string)
#                policy1 = DeliveryPlanner.moves_plan(self, warehouse, directions, delta_name, moves_costs, start_point, end_point, packet)
#                print policy1
#                moves1 = DeliveryPlanner.policy_string(self,policy1,directions,moves,start_point, end_point, packet, target, curr_target,delta_name)
                    robot_start = policy[j]
                string = "lift " + str(self.todo[ind])
                moves.append(string)


                policy.reverse()
                temp_grid = list(grid[self.classic_todo[ind][0]])
                temp_grid[self.classic_todo[ind][1]] = '.'
                grid[self.classic_todo[ind][0]] = ''.join(temp_grid)

                
                

                for j in range(1, len(policy)):
                    if abs(policy[j][0] - self.dropzone[0]) == 0 and abs(policy[j][1] - self.dropzone[1]) == 0:
                        break
#                policy1 = DeliveryPlanner.moves_plan(self, warehouse, directions, delta_name, moves_costs, start_point, end_point, packet)
#                print policy1
#                moves1 = DeliveryPlanner.policy_string(self,policy1,directions,moves,start_point, end_point, packet, target, curr_target,delta_name)                    
                    string = "move " + str(policy[j][0]) + " " + str(policy[j][1])
                    moves.append(string)

                    robot_start = policy[j]
                last_pos = policy[-1]
                
                if robot_start[0] == self.dropzone[0] and robot_start[1] == self.dropzone[1]:
                    for k in range(len(self.delta)):
                        robot_pos = (robot_start[0] + self.delta[k][0], robot_start[1] + self.delta[k][1])
                        if robot_pos[0] >= 0 and robot_pos[0] < len(grid) and robot_pos[1] >= 0 and robot_pos[1] < len(grid[0]) and grid[robot_pos[0]][robot_pos[1]] == '.':
                            robot_start = robot_pos
                    string = "move " + str(robot_start[0]) + " " + str(robot_start[1])
                    moves.append(string)

                if abs(last_pos[0] - self.dropzone[0]) > 1 or abs(last_pos[1] - self.dropzone[1]) > 1:
                    policy, robot_start = self.astar(heuristic, robot_start, grid, self.dropzone)
                    for j in range(0, len(policy)):
                        string = "move " + str(policy[j][0]) + " " + str(policy[j][1])
                        moves.append(string)
#                policy1 = DeliveryPlanner.moves_plan(self, warehouse, directions, delta_name, moves_costs, start_point, end_point, packet)
#                print policy1
#                moves1 = DeliveryPlanner.policy_string(self,policy1,directions,moves,start_point, end_point, packet, target, curr_target,delta_name)
                        robot_start = policy[j]
                string = "down " + str(self.dropzone[0]) + " " + str(self.dropzone[1])
                moves.append(string)
            else:
#            if self.adjacent_cell(item, robot_start):
                prev_pos = robot_start
                temp_grid = list(grid[self.classic_todo[ind][0]])
                temp_grid[self.classic_todo[ind][1]] = '.'
                grid[self.classic_todo[ind][0]] = ''.join(temp_grid)
                
                string = "lift " + str(self.todo[ind])
                moves.append(string)

#                start_point = dropzone
#                target = 0
#                warehouse = self.warehouse
#                print 'packet pickup'
#                packet = 0
#                policy = DeliveryPlanner.moves_plan(self, warehouse, directions, delta_name, moves_costs, start_point, end_point, packet)
#                print policy
#                moves = DeliveryPlanner.policy_string(self,policy,directions,moves,start_point, end_point, packet, target, curr_target,delta_name)
#                policy1 = DeliveryPlanner.moves_plan(self, warehouse, directions, delta_name, moves_costs, start_point, end_point, packet)
#                print policy1
#                moves1 = DeliveryPlanner.policy_string(self,policy1,directions,moves,start_point, end_point, packet, target, curr_target,delta_name)

                
                
#                if ind + 1 > len(self.classic_todo):
                if ind < len(self.classic_todo) - 1:
                    
                    

                    min_dist = 999
                    next_box = self.classic_todo[ind + 1]
                    for k in range(len(self.delta)):
                        robot_pos = (robot_start[0] + self.delta[k][0], robot_start[1] + self.delta[k][1])
                        dist = max(abs(robot_pos[0] - next_box[0]),abs(robot_pos[1] - next_box[1]))
                        
                        if robot_pos[0] >= 0 and robot_pos[0] < len(grid) and robot_pos[1] >= 0 and robot_pos[1] < len(grid[0]) and self.warehouse[robot_pos[0]][robot_pos[1]] == '.' and dist < min_dist:
                            
                            min_dist = dist
                            robot_start = robot_pos
                    # print robot_start
                else:
                   
                    for k in range(len(self.delta)):
                        robot_pos = (robot_start[0] + self.delta[k][0], robot_start[1] + self.delta[k][1])
                        if robot_pos[0] >= 0 and robot_pos[0] < len(grid) and robot_pos[1] >= 0 and robot_pos[1] < len(grid[0]) and self.warehouse[robot_pos[0]][robot_pos[1]] == '.':
                            robot_start = robot_pos
                    
                if prev_pos[1] != robot_start[1] or prev_pos[0]!=robot_start[0]:
                    
                    string = "move " + str(robot_start[0]) + " " + str(robot_start[1])
                    moves.append(string)
                
                if self.adjacent_cell(robot_start, self.dropzone)== True:
#                policy1 = DeliveryPlanner.moves_plan(self, warehouse, directions, delta_name, moves_costs, start_point, end_point, packet)
#                print policy1
#                moves1 = DeliveryPlanner.policy_string(self,policy1,directions,moves,start_point, end_point, packet, target, curr_target,delta_name)

                    if robot_start[0] == self.dropzone[0] and robot_start[1] == self.dropzone[1]:
                        for k in range(len(self.delta)):
                            robot_pos = (robot_start[0] + self.delta[k][0], robot_start[1] + self.delta[k][1])
                            if robot_pos[0] >= 0 and robot_pos[0] < len(grid) and robot_pos[1] >= 0 and robot_pos[1] < len(grid[0]) and grid[robot_pos[0]][robot_pos[1]] == '.':
                                robot_start = robot_pos
                        string = "move "+str(robot_start[0])+" "+str(robot_start[1])
                        moves.append(string)
#                        
                    string = "down " + str(self.dropzone[0]) + " " + str(self.dropzone[1])
                    moves.append(string)
                    
                else:
#                packet = 0
#                policy = DeliveryPlanner.moves_plan(self, warehouse, directions, delta_name, moves_costs, start_point, end_point, packet)
#                print policy
#                moves = DeliveryPlanner.policy_string(self,policy,directions,moves,start_point, end_point, packet, target, curr_target,delta_name)
#                
                    policy, robot_start = self.astar(heuristic, robot_start, grid, self.dropzone)
                    # print "in 1 astar", policy
                    for j in range(len(policy)):
                        string = "move " + str(policy[j][0]) + " " + str(policy[j][1])
                        moves.append(string)
                        robot_start = policy[j]
                        
                        
#                packet = 0
#                policy = DeliveryPlanner.moves_plan(self, warehouse, directions, delta_name, moves_costs, start_point, end_point, packet)
#                print policy
#                moves = DeliveryPlanner.policy_string(self,policy,directions,moves,start_point, end_point, packet, target, curr_target,delta_name)
#                
                  
                    if robot_start[0] == self.dropzone[0] and robot_start[1] == self.dropzone[1]:
                        for k in range(len(self.delta)):
                            robot_pos = (robot_start[0] + self.delta[k][0], robot_start[1] + self.delta[k][1])
                            if robot_pos[0] >= 0 and robot_pos[0] < len(grid) and robot_pos[1] >= 0 and robot_pos[1] < len(grid[0]) and grid[robot_pos[0]][robot_pos[1]] == '.':
                                robot_start = robot_pos
                        string = "move "+str(robot_start[0])+" "+str(robot_start[1])
                        moves.append(string)
#                        
                    string = "down " + str(self.dropzone[0]) + " " + str(self.dropzone[1])
                    moves.append(string)
#                    
                
#                    

                # print moves
            

#            final_output = []
#                final_output.append(moves[0])
#                t = 1
#                for i in range(1,len(moves)):
#                    #final_output[t] = moves1[i]
#                    if moves[i-1][:4] == 'lift' or  moves[i-1][:4] == 'down':
#                        if moves[i-2] == moves[i]:
#                            continue
#                        elif moves[i-2] != moves[i]:
#                            prev_pos = (int(moves[i-2][5]),int(moves[i-2][7]))
#                            curr_pos = (int(moves[i][5]),int(moves[i][7]))
#                            if (abs(prev_pos[0] - curr_pos[0]) > 1 or abs(prev_pos[1] - curr_pos[1]) > 1):
#                                string = 'move ' + str(self.robot_start[0]) + ' ' + str(self.robot_start[1])
#                                final_output.append(string)
#                                final_output.append(moves[i])
#                            else:
#                                final_output.append(moves[i])
#                            #t = t+1
#                    else:
#                        final_output.append(moves[i])
#                        t = t+1
#            final_output = moves
                # print moves, robot_start


        return moves
    
    def moves_plan(self, warehouse, directions, delta_name, moves_costs, start_point, end_point, packet):
        x0 = start_point[0]
        y0 = start_point[1]
        
        h = [[99 for x in range(len(warehouse[0]))] for y in range(len(warehouse))]
        policy = [[' ' for row in range(len(warehouse[0]))] for col in range(len(warehouse))]
        change = True
        while change:
            change = False
            for x in range(len(warehouse)):
                for y in range(len(warehouse[0])):
                    if y == end_point[1] and x == end_point[0] and h[x][y] == 99:
                        if h[x][y] > 0:
                            h[x][y] = 0
                            change = True
                            policy[x][y] = '*'
                                                
                    #if warehouse[x][y] == '.' or (x == x0 and y == y0):
                    if (warehouse[x][y] != '#' and warehouse[x][y] != '@') or (x == x0 and y == y0):
                        for dir in range(len(directions)):
                            x1 = x + directions[dir][0]
                            y1 = y + directions[dir][1]
                            
                            if x1 >= 0 and x1 < len(warehouse) and y1 >= 0 and y1 < len(warehouse[0]):
                                #print 'len warehouse =' + str(len(warehouse[0])) + ',' + str(len(warehouse))
                                #print 'old coordinates = ' + str(x) + ',' + str(y)
                                #print '(x1, y1) = ' + str(x1) + ',' + str(y1)
                                #print 'y1 = ' + str(y1)
                                c1 = h[x1][y1] + moves_costs[dir]
                                
                                if c1 < h[x][y]:
                                    #print 'done'
                                    h[x][y] = c1
                                    change = True
                                    policy[x][y] = delta_name[dir]
        
        #print h
        return policy
    
    def adjacent_cell(self, pos, goal):
        if abs(pos[0] - goal[0]) <= 1 and abs(pos[1] - goal[1]) <= 1:
            return True
        else:
            return False
        
        
        
    def astar(self, heuristic, init, grid, goal):

        # internal moves parameters
        delta = [[-1, 0],  # go up
                 [0, -1],  # go left
                 [1, 0],  # go down
                 [0, 1],  # do right
                 [1, 1],  # top right
                 [1, -1],  # bottom right
                 [-1, 1],  # bottom left
                 [-1, -1]]  # top left

        # open list elements are of the type: [f, g, h, x, y]

        expand = [[0 for row in range(len(self.warehouse[0]))]
                  for col in range(len(self.warehouse))]
        action = [[0 for row in range(len(self.warehouse[0]))]
                  for col in range(len(self.warehouse))]

        expand[init[0]][init[1]] = 1

        x = init[0]
        y = init[1]
        h = heuristic[x][y]
        g = 0
        f = g + h

        open = [[f, g, h, x, y]]

        found = False  # flag that is set when search complete
        resign = False  # flag set if we can't find expand
        count = 0

        while not found and not resign:

            # check if we still have elements on the open list
            if len(open) == 0:
                resign = True
                print '###### Search terminated without success'

            else:
                # remove node from list
                open.sort()
                open.reverse()
                next = open.pop()
                x = next[3]
                y = next[4]
                g = next[1]


            if abs(x - goal[0]) <= 1 and abs(y - goal[1]) <= 1:
                for k in range(len(delta)):
                    if x + delta[k][0] == goal[0] and y + delta[k][1] == goal[1]:
 
                        action[goal[0]][goal[1]] = k
                found = True
            else:
   
                for i in range(len(delta)):
                    x2 = x + delta[i][0]
                    y2 = y + delta[i][1]
                    if x2 >= 0 and x2 < len(grid) and y2 >= 0 \
                            and y2 < len(grid[0]):
                        if expand[x2][y2] == 0 and (grid[x2][y2] == '.' or grid[x2][y2] == '@'):
                            if i > 4:
                                g2 = g + self.cost_diagonal
                            else:
                                g2 = g + self.cost_horizontal

                            h2 = heuristic[x2][y2]
                            f2 = g2 + h2
                            open.append([f2, g2, h2, x2, y2])
                            expand[x2][y2] = 1
                            action[x2][y2] = i

            count += 1



        x = goal[0]
        y = goal[1]
        rev = []
        rev.append([x, y])
        while x != init[0] or y != init[1]:
            x2 = x - delta[action[x][y]][0]
            y2 = y - delta[action[x][y]][1]
            x = x2
            y = y2
            rev.append([x, y])

        policy = []
        # print "init", init
        # print "goal", goal
        for i in range(0, len(rev)):
            if ((rev[len(rev) - 1 - i][0] == init[0]) and (rev[len(rev) - 1 - i][1] == init[1]))==False and  ((rev[len(rev) - 1 - i][0] == goal[0]) and (rev[len(rev) - 1 - i][1] == goal[1]))==False:
            
                policy.append(rev[len(rev) - 1 - i])

        # print "policy " + str(policy)
        # print policy[0]
        return policy, policy[0]



    def define_heuristic(self, classic_todo, init):
        heuristic = [[0 for row in range(self.cols)]
                     for col in range(self.rows)]

        for ind in range(len(classic_todo)):
            item = classic_todo[ind]
            
            heuristic[item[0]][item[1]] = max(abs(init[0] - item[0]),abs(init[1] - item[1]))
            

        return heuristic
    
    
    def classic_todo_func(self, todo, warehouse):
        # print(self.rows, self.cols)
        todo_list = []
        for item in range(len(todo)):
            for i in range(self.rows):
                found = False
                for j in range(self.cols):
                    # print i,j
                    if warehouse[i][j] == todo[item]:
                        todo_list.append((i, j))
                        found = True
                        break
                if found:
                    break
        return todo_list



   

