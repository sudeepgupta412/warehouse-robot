#
# === Introduction ===
#
# In this problem, you will again build a planner that helps a robot
#   find the best path through a warehouse filled with boxes
#   that it has to pick up and deliver to a dropzone. Unlike Part A,
#   however, in this problem the robot is moving in a continuous world
#   (albeit in discrete time steps) and has constraints on the amount
#   it can turn its wheels in a given time step.
# 
# Your file must be called `partB.py` and must have a class
#   called `DeliveryPlanner`.
# This class must have an `__init__` function that takes five 
#   arguments: `self`, `warehouse`, `todo`, `max_distance`, and
#   `max_steering`.
# The class must also have a function called `plan_delivery` that 
#   takes a single argument, `self`.
#
# === Input Specifications ===
# 
# `warehouse` will be a list of m strings, each with n characters,
#   corresponding to the layout of the warehouse. The warehouse is an
#   m x n grid. warehouse[i][j] corresponds to the spot in the ith row
#   and jth column of the warehouse, where the 0th row is the northern
#   end of the warehouse and the 0th column is the western end.
#
# The characters in each string will be one of the following:
#
# '.' (period) : traversable space.
# '#' (hash) : a wall. If the robot contacts a wall space, it will crash.
# '@' (dropzone): the space where all boxes must be delivered. The dropzone may be traversed like 
#   a '.' space.
#
# Each space is a 1 x 1 block. The upper-left corner of space warehouse[i][j] is at the point (j,-i) in
#   the plane. Spaces outside the warehouse are considered walls; if any part of the robot leaves the 
#   warehouse, it will be considered to have crashed into the exterior wall of the warehouse.
# 
# For example, 
#   warehouse = ['.#.',
#                '.#.',
#                '..@']
#   is a 3x3 warehouse. The dropzone is at space (2,-2) and there are walls at spaces (1,0) 
#   and (1,-1). The rest of the warehouse is empty space.
#
# The robot is a circle of radius 0.25. The robot begins centered in the dropzone space.
#   The robot's initial bearing is 0.
#
# The argument `todo` is a list of points representing the center point of each box.
#   todo[0] is the first box which must be delivered, followed by todo[1], and so on.
#   Each box is a square of size 0.2 x 0.2. If the robot contacts a box, it will crash.
#
# The arguments `max_distance` and `max_steering` are parameters constraining the movement
#   of the robot on a given time step. They are described more below.
#
# === Rules for Movement ===
#
# - The robot may move any distance between 0 and `max_distance` per time step.
# - The robot may set its steering angle anywhere between -`max_steering` and 
#   `max_steering` per time step. A steering angle of 0 means that the robot will
#   move according to its current bearing. A positive angle means the robot will 
#   turn counterclockwise by `steering_angle` radians; a negative steering_angle 
#   means the robot will turn clockwise by abs(steering_angle) radians.
# - Upon a movement, the robot will change its steering angle instantaneously to the 
#   amount indicated by the move, and then it will move a distance in a straight line in its
#   new bearing according to the amount indicated move.
# - The cost per move is 1 plus the amount of distance traversed by the robot on that move.
#
# - The robot may pick up a box whose center point is within 0.5 units of the robot's center point.
# - If the robot picks up a box, it incurs a total cost of 2 for that move (this already includes 
#   the 1-per-move cost incurred by the robot).
# - While holding a box, the robot may not pick up another box.
# - The robot may put a box down at a total cost of 1.5 for that move. The box must be placed so that:
#   - The box is not contacting any walls, the exterior of the warehouse, any other boxes, or the robot
#   - The box's center point is within 0.5 units of the robot's center point
# - A box is always oriented so that two of its edges are horizontal and the other two are vertical.
# - If a box is placed entirely within the '@' space, it is considered delivered and is removed from the 
#   warehouse.
# - The warehouse will be arranged so that it is always possible for the robot to move to the 
#   next box on the todo list without having to rearrange any other boxes.
#
# - If the robot crashes, it will stop moving and incur a cost of 100*distance, where distance
#   is the length it attempted to move that move. (The regular movement cost will not apply.)
# - If an illegal move is attempted, the robot will not move, but the standard cost will be incurred.
#   Illegal moves include (but are not necessarily limited to):
#     - picking up a box that doesn't exist or is too far away
#     - picking up a box while already holding one
#     - putting down a box too far away or so that it's touching a wall, the warehouse exterior, 
#       another box, or the robot
#     - putting down a box while not holding a box
#
# === Output Specifications ===
#
# `plan_delivery` should return a LIST of strings, each in one of the following formats.
#
# 'move {steering} {distance}', where '{steering}' is a floating-point number between
#   -`max_steering` and `max_steering` (inclusive) and '{distance}' is a floating-point
#   number between 0 and `max_distance`
# 
# 'lift {b}', where '{b}' is replaced by the index in the list `todo` of the box being picked up
#   (so if you intend to lift box 0, you would return the string 'lift 0')
#
# 'down {x} {y}', where '{x}' is replaced by the x-coordinate of the center point of where the box
#   will be placed and where '{y}' is replaced by the y-coordinate of that center point
#   (for example, 'down 1.5 -2.9' means to place the box held by the robot so that its center point
#   is (1.5,-2.9)).
#
# === Grading ===
# 
# - Your planner will be graded against a set of test cases, each equally weighted.
# - Each task will have a "baseline" cost. If your set of moves results in the task being completed
#   with a total cost of K times the baseline cost, you will receive 1/K of the credit for the
#   test case. (Note that if K < 1, this means you earn extra credit!)
# - Otherwise, you will receive no credit for that test case. This could happen for one of several 
#   reasons including (but not necessarily limited to):
#   - plan_delivery's moves do not deliver the boxes in the correct order.
#   - plan_delivery's output is not a list of strings in the prescribed format.
#   - plan_delivery does not return an output within the prescribed time limit.
#   - Your code raises an exception.
#
# === Additional Info ===
# 
# - You may add additional classes and functions as needed provided they are all in the file `partB.py`.
# - Your partB.py file must not execute any code when it is imported. 
# - Upload partB.py to Project 2 on T-Square in the Assignments section. Do not put it into an 
#   archive with other files.
# - Ask any questions about the directions or specifications on Piazza.
#

from robot import *
import numpy as np
import math

class DeliveryPlanner:
    def __init__(self, warehouse, todo, max_distance, max_steering):
        self.warehouse = warehouse
        self.todo = todo
        self.max_distance = max_distance
        self.max_steering = max_steering
        self.min_steering = -1 * max_steering

        self.grid_division = 1
        self.discrete_warehouse = self.partA_warehouse(self.warehouse, self.todo)
        self.unit_dist_diagonal = math.sqrt(2 * (float(1. / self.grid_division) * float(1. / self.grid_division)))
        self.unit_dist_non_diag = float(1. / self.grid_division)

        self.rows = len(self.discrete_warehouse)
        self.cols = len(self.discrete_warehouse[0])
        self.classic_todo = self.classic_todo_func(self.discrete_warehouse, len(self.todo))
        self.init = self.initialize_robot(self.discrete_warehouse)
        self.dropzone = self.init
        self.cost_horizontal = 2
        self.cost_diagonal = 3
        self.cost_vertical = 2
        self.heuristic = self.define_heuristic(self.classic_todo, self.init)
        self.moves_costs =  [2, 2, 2, 2, 3, 3, 3, 3]
        self.delta_name = ['u', 'l','d','r','u-r','d-r','d-l','u-l']
        self.delta = [[-1, 0],  # go up
                      [0, -1],  # go left
                      [1, 0],  # go down
                      [0, 1],  # do right
                      [1, 1],  # top right
                      [1, -1],  # bottom right
                      [-1, 1],  # bottom left
                      [-1, -1]]  # top left

    
    def policy_string(self,policy,directions,moves,start_point, end_point, packet, target, curr_packet,delta_name):
        x0 = start_point[0]
        y0 = start_point[1]
        self.previous = start_point
        #self.coordinates = []
        #print 'packet status = ' + str(packet) + ' ' + str(end_point)
        x,y = x0,y0
        i = 0
        while ((x == end_point[0]) and (y == end_point[1])) != 1:
            #print y
            i = i + 1
            for delta in range(len(delta_name)):
                if policy[x][y] == delta_name[delta]:
                    string = ''
                    x = x + directions[delta][0]
                    y = y + directions[delta][1]
                    
                    #print 'packet =' + str(packet) + ' coordinates = ' + str((x,y))
                    if (packet == 0): 
                        if (x == end_point[0] and y == end_point[1]):
                            string = 'lift ' + str(curr_packet)
                            
                        else:
                            string = 'move ' + str(x) + ' ' + str(y)
                            self.previous = (x,y)
                    elif (packet == 1) :
                        if (x == end_point[0] and y == end_point[1]):
                            string = 'down ' + str(x) + ' ' + str(y)
                        else:
                            string = 'move ' + str(x) + ' ' + str(y)
                            self.previous = (x,y)
                    #print moves
                    if packet == 1 and i == 1:
                        #print 'I am at this locaotin' + str(x) + ',' + str(y)
                        break
                    else:
                        moves.append(string)
                        
                        break
                    
        
#        if (packet == 0) :
#            string = 'lift ' + str(curr_packet)
#            moves.append(string)
#        if (packet == 1) :
#            string = 'down ' + str(end_point[0]) + ' ' + str(end_point[1])  
#            moves.append(string)
        #print self.coordinates    
        return moves
    
    
    def adjacent_cell(self, pos, goal):
        if abs(pos[0] - goal[0]) <= 1 and abs(pos[1] - goal[1]) <= 1:
            return True
        else:
            return False
    
    def calculate_distance(self, d1, d2):
        return self.round_float(round(math.sqrt((d1[0] - d2[0])**2 + (d1[1] - d2[1])**2 )), 2)
            
    def plan_delivery(self):
        # Sample of what a moves list should look like - replace with your planner results

        grid = self.discrete_warehouse
        #string = ''
        
        heuristic = self.heuristic
        robot_start = self.dropzone

        moves = []
        #item = self.classic_todo.pop(0)
        #print item
        
        for ind in range(len(self.classic_todo)):
            item = self.classic_todo[ind]
            #print item
            
            if not self.adjacent_cell(item, robot_start):
                
                
                policy, robot_start = self.astar(heuristic, robot_start, grid, item)
                # print "pick_policy", policy
                for j in range(len(policy)):
                    
                    #string = "move " + str(policy[j][0]) + " " + str(policy[j][1])
                    #string = "move " + str(policy[j][0]) + " " + str(policy[j][1])
                    moves.append(("move", (policy[j][0], policy[j][1])))
#                policy1 = DeliveryPlanner.moves_plan(self, warehouse, directions, delta_name, moves_costs, start_point, end_point, packet)
#                print policy1
#                moves1 = DeliveryPlanner.policy_string(self,policy1,directions,moves,start_point, end_point, packet, target, curr_target,delta_name)
                    robot_start = policy[j]
                #string = "lift " + str(self.todo[ind])
                moves.append(("lift", str(ind)))


                policy.reverse()
                temp_grid = list(grid[self.classic_todo[ind][0]])
                #temp_grid[self.classic_todo[ind][1]] = '.'
                #grid[self.classic_todo[ind][0]] = ''.join(temp_grid)

                
                

                for j in range(1, len(policy)):
                    
                    if abs(policy[j][0] - self.dropzone[0]) == 0 and abs(policy[j][1] - self.dropzone[1]) == 0:
                        break
#                policy1 = DeliveryPlanner.moves_plan(self, warehouse, directions, delta_name, moves_costs, start_point, end_point, packet)
#                print policy1
#                moves1 = DeliveryPlanner.policy_string(self,policy1,directions,moves,start_point, end_point, packet, target, curr_target,delta_name)                    
                    #string = "move " + str(policy[j][0]) + " " + str(policy[j][1])
                    moves.append(("move", (policy[j][0], policy[j][1])))

                    robot_start = policy[j]
                #last_pos = policy[-1]
                
                    #policy, robot_start = self.astar(heuristic, robot_start, grid, self.dropzone)
                
                if robot_start[0] == self.dropzone[0] and robot_start[1] == self.dropzone[1]:
                    
                    for k in range(len(self.delta)):
                        
                        robot_pos = (robot_start[0] + self.delta[k][0], robot_start[1] + self.delta[k][1])
                        if robot_pos[0] >= 0 and robot_pos[0] < len(grid) and robot_pos[1] >= 0 and robot_pos[1] < len(grid[0]) and grid[robot_pos[0]][robot_pos[1]] == '.':
                            robot_start = robot_pos
                    #string = "move " + str(robot_start[0]) + " " + str(robot_start[1])
                    moves.append(("move", (robot_start[0], robot_start[1])))
                
                print 'hi'
                print 'hi'
                if self.calculate_distance(robot_start, self.dropzone) > 1.0:
                    print 'hi'
                    policy, robot_start = self.astar(heuristic, robot_start, grid, self.dropzone)
                    for j in range(0, len(policy)):
                        #string = "move " + str(policy[j][0]) + " " + str(policy[j][1])
                        moves.append(("move", (policy[j][0], policy[j][1])))
#                policy1 = DeliveryPlanner.moves_plan(self, warehouse, directions, delta_name, moves_costs, start_point, end_point, packet)
#                print policy1
#                moves1 = DeliveryPlanner.policy_string(self,policy1,directions,moves,start_point, end_point, packet, target, curr_target,delta_name)
                        robot_start = policy[j]
                #string = "down " + str(self.dropzone[0]) + " " + str(self.dropzone[1])
                moves.append(("down", (self.dropzone[0], self.dropzone[1])))
                print 'hi'
            else:
                
#            if self.adjacent_cell(item, robot_start):
                prev_pos = robot_start
                
                grid[self.classic_todo[ind][0]][self.classic_todo[ind][1]] = '.'
                
                #string = "lift " + str(self.todo[ind])
                moves.append(("lift" , str(ind)))

#                start_point = dropzone
#                target = 0
#                warehouse = self.warehouse
#                print 'packet pickup'
#                packet = 0
#                policy = DeliveryPlanner.moves_plan(self, warehouse, directions, delta_name, moves_costs, start_point, end_point, packet)
#                print policy
#                moves = DeliveryPlanner.policy_string(self,policy,directions,moves,start_point, end_point, packet, target, curr_target,delta_name)
#                policy1 = DeliveryPlanner.moves_plan(self, warehouse, directions, delta_name, moves_costs, start_point, end_point, packet)
#                print policy1
#                moves1 = DeliveryPlanner.policy_string(self,policy1,directions,moves,start_point, end_point, packet, target, curr_target,delta_name)

                
                
#                if ind + 1 > len(self.classic_todo):
                if ind < len(self.classic_todo) - 1:
                    
                    

                    min_dist = 1000
                    next_box = self.classic_todo[ind + 1]
                    for k in range(len(self.delta)):
                        robot_pos = (robot_start[0] + self.delta[k][0], robot_start[1] + self.delta[k][1])
                        dist = max(abs(robot_pos[0] - next_box[0]),abs(robot_pos[1] - next_box[1]))
                        
                        if robot_pos[0] >= 0 and robot_pos[0] < len(grid) and robot_pos[1] >= 0 and robot_pos[1] < len(grid[0]) and self.warehouse[robot_pos[0]][robot_pos[1]] == '.' and dist < min_dist:
                            
                            min_dist = dist
                            robot_start = robot_pos
                    # print robot_start
                else:
                   
                    for k in range(len(self.delta)):
                        robot_pos = (robot_start[0] + self.delta[k][0], robot_start[1] + self.delta[k][1])
                        if robot_pos[0] >= 0 and robot_pos[0] < len(grid) and robot_pos[1] >= 0 and robot_pos[1] < len(grid[0]) and self.warehouse[robot_pos[0]][robot_pos[1]] == '.':
                            robot_start = robot_pos
                    
                if prev_pos[1] != robot_start[1] or prev_pos[0]!=robot_start[0]:
                    
                    #string = "move " + str(robot_start[0]) + " " + str(robot_start[1])
                    moves.append(("move",(robot_start[0], robot_start[1])))
                
                if self.adjacent_cell(robot_start, self.dropzone)== True:
#                policy1 = DeliveryPlanner.moves_plan(self, warehouse, directions, delta_name, moves_costs, start_point, end_point, packet)
#                print policy1
#                moves1 = DeliveryPlanner.policy_string(self,policy1,directions,moves,start_point, end_point, packet, target, curr_target,delta_name)

                    if robot_start[0] == self.dropzone[0] and robot_start[1] == self.dropzone[1]:
                        for k in range(len(self.delta)):
                            robot_pos = (robot_start[0] + self.delta[k][0], robot_start[1] + self.delta[k][1])
                            if robot_pos[0] >= 0 and robot_pos[0] < len(grid) and robot_pos[1] >= 0 and robot_pos[1] < len(grid[0]) and grid[robot_pos[0]][robot_pos[1]] == '.':
                                robot_start = robot_pos
                        #string = "move "+str(robot_start[0])+" "+str(robot_start[1])
                        moves.append(("move", (robot_start[0],robot_start[1])))
#                        
                    #string = "down " + str(self.dropzone[0]) + " " + str(self.dropzone[1])
                    moves.append(("down", self.dropzone))
                    
                else:
#                packet = 0
#                policy = DeliveryPlanner.moves_plan(self, warehouse, directions, delta_name, moves_costs, start_point, end_point, packet)
#                print policy
#                moves = DeliveryPlanner.policy_string(self,policy,directions,moves,start_point, end_point, packet, target, curr_target,delta_name)
#                
                    policy, robot_start = self.astar(heuristic, robot_start, grid, self.dropzone)
                    # print "in 1 astar", policy
                    for j in range(len(policy)):
                        #string = "move " + str(policy[j][0]) + " " + str(policy[j][1])
                        moves.append(("move", (policy[j][0], policy[j][1])))
                        robot_start = policy[j]
                        
                        
#                packet = 0
#                policy = DeliveryPlanner.moves_plan(self, warehouse, directions, delta_name, moves_costs, start_point, end_point, packet)
#                print policy
#                moves = DeliveryPlanner.policy_string(self,policy,directions,moves,start_point, end_point, packet, target, curr_target,delta_name)
#                
                  
                    if robot_start[0] == self.dropzone[0] and robot_start[1] == self.dropzone[1]:
                        for k in range(len(self.delta)):
                            robot_pos = (robot_start[0] + self.delta[k][0], robot_start[1] + self.delta[k][1])
                            if robot_pos[0] >= 0 and robot_pos[0] < len(grid) and robot_pos[1] >= 0 and robot_pos[1] < len(grid[0]) and grid[robot_pos[0]][robot_pos[1]] == '.':
                                robot_start = robot_pos
                        #string = "move "+str(robot_start[0])+" "+str(robot_start[1])
                        moves.append(("move",(robot_start[0],robot_start[1])))
#                        
                    #string = "down " + str(self.dropzone[0]) + " " + str(self.dropzone[1])
                    moves.append(("down", self.dropzone))

                # print moves, curr_start
                # print "==================================================="

        # print "before init", self.init
            # print "before init", self.init
        self.init = self.dropzone
        init = (self.init[1] / float(self.grid_division), -self.init[0] / float(self.grid_division))
        # print "init", init
        # print moves
        
        centre_init = (init[0] + 0.5, init[1] - 0.5)
        state = State(self.warehouse, self.todo, self.max_distance, self.max_steering, centre_init)
        #print state
        continuous_move = []
        centre_moves = []
        for i in range(len(moves)):
            if moves[i][0] != 'lift':
                continuous_move.append((moves[i][0], (moves[i][1][1] / float(self.grid_division), -1 * (moves[i][1][0] / float(self.grid_division)))))
                centre_moves.append((continuous_move[i][0], (continuous_move[i][1][0] + 0.5, continuous_move[i][1][1] - 0.5)))
            else:
                continuous_move.append(moves[i])
                centre_moves.append(continuous_move[i])

        
        out_moves, list_xy = self.process_moves(centre_init, centre_moves)
        # print "out_moves", out_moves
        # print "list_xy", list_xy
        
        
        min_moves = state.minimize_steps(out_moves, list_xy)
        #min_moves = self.minimize_steps1(out_moves, list_xy)
        return min_moves




    def partA_warehouse(self, warehouse, todo):
        # print todo
        discrete_warehouse = [[0 for row in range(len(self.warehouse[0]))]
                       for col in range(len(self.warehouse))]

        for i in range(len(self.warehouse)):
            for j in range(len(self.warehouse[i])):
                discrete_warehouse[i][j] = str(self.warehouse[i][j])

        # mark boxes now
        get_new_coord = lambda x: (int(math.fabs(x[1])), int(x[0]))
        for i in range(len(todo)):
            x, y = get_new_coord(todo[i])
            discrete_warehouse[x][y] = str(i)

        return discrete_warehouse

    def astar(self, heuristic, init, grid, goal):

        # internal motion parameters
        delta = [[-1, 0],  # go up
                 [0, -1],  # go left
                 [1, 0],  # go down
                 [0, 1]]  # do right
        # [1, 1],  # top right
        # [1, -1],  # bottom right
        # [-1, 1],  # bottom left
        # [-1, -1]]  # top left

        # open list elements are of the type: [f, g, h, x, y]

        expand = [[0 for row in range(len(self.discrete_warehouse[0]))]
                  for col in range(len(self.discrete_warehouse))]
        action = [[0 for row in range(len(self.discrete_warehouse[0]))]
                  for col in range(len(self.discrete_warehouse))]

        expand[init[0]][init[1]] = 1

        x = init[0]
        y = init[1]
        h = heuristic[x][y]
        g = 0
        f = g + h

        open = [[f, g, h, x, y]]

        found = False  # flag that is set when search complete
        resign = False  # flag set if we can't find expand
        count = 0

        while not found and not resign:

            # check if we still have elements on the open list
            if len(open) == 0:
                resign = True
                # print '###### Search terminated without success'
                pass

            else:
                # remove node from list
                open.sort()
                open.reverse()
                next = open.pop()
                x = next[3]
                y = next[4]
                g = next[1]

            # check if we are done

            if (abs(x - goal[0]) == 1 and abs(y - goal[1]) == 0) or (abs(x - goal[0]) == 0 and abs(y - goal[1]) == 1):
                # print "found1", goal[0], goal[1]
                for k in range(len(delta)):
                    if x + delta[k][0] == goal[0] and y + delta[k][1] == goal[1]:
                        # print "found2", goal[0], goal[1]
                        action[goal[0]][goal[1]] = k
                found = True
            else:
                # expand winning element and add to new open list
                for i in range(len(delta)):
                    x2 = x + delta[i][0]
                    y2 = y + delta[i][1]

                    if x2 >= 0 and x2 < len(grid) and y2 >= 0 \
                            and y2 < len(grid[0]):
                        if expand[x2][y2] == 0 and \
                                (grid[x2][y2] == "." or grid[x2][y2] == "@"):
                            if i > 4:
                                g2 = g + self.cost_diagonal
                            else:
                                g2 = g + self.cost_horizontal

                            h2 = heuristic[x2][y2]
                            f2 = g2 + h2
                            open.append([f2, g2, h2, x2, y2])
                            expand[x2][y2] = 1
                            action[x2][y2] = i

            count += 1

        # extract the path


        # print action

        rev = []
        x = goal[0]
        y = goal[1]
        # print "x, y ", x, y
        rev.append([x, y])
        while x != init[0] or y != init[1]:
            # print x, y, delta[action[x][y]][0], delta[action[x][y]][1]
            x2 = x - delta[action[x][y]][0]
            y2 = y - delta[action[x][y]][1]
            x = x2
            y = y2
            rev.append([x, y])

        path = []
        # print "init", init
        # print "goal", goal
        for i in range(0, len(rev)):
            if ((rev[len(rev) - 1 - i][0] == init[0]) and (rev[len(rev) - 1 - i][1] == init[1]))==False and  ((rev[len(rev) - 1 - i][0] == goal[0]) and (rev[len(rev) - 1 - i][1] == goal[1]))==False:
                # print invpath[len(invpath) - 1 - i]
                path.append(rev[len(rev) - 1 - i])

        # print "path: ", path
        return path, path[0]

    

    def is_similar_pos(self, p1, p2):
        if p1[0] == p2[0] and p1[1] == p2[1]:
            return True
        else:
            return False

    def classic_todo_func(self, warehouse, todo_length):
        
        todo_classic = []
        for lst in range(todo_length):
            for i in range(self.rows):
                
                for j in range(self.cols):
                    if warehouse[i][j] == str(lst):
                        todo_classic.append((i, j))
                        
                        break
#                
        return todo_classic

    def initialize_robot(self, warehouse):

        for j in range(len(warehouse[0])):
            for i in range(len(warehouse)):
                if warehouse[i][j] == '@':
                    return i, j

    def manhattan_distance(self, pos, goal):
        # print pos, goal
        return abs(pos[0] - goal[0]) + abs(pos[1] - goal[1])

    def define_heuristic(self, classic_todo, init):
        heuristic = [[0 for row in range(self.cols)]
                     for col in range(self.rows)]

        for ind in range(len(classic_todo)):
            item = classic_todo[ind]
            
            heuristic[item[0]][item[1]] = max(abs(init[0] - item[0]),abs(init[1] - item[1]))
            

        return heuristic

    def get_heading(self, p1, p2):
        heading = math.atan2(p2[1] - p1[1], p2[0] - p1[0])
        return heading

    def get_direction(self, r, p1, p2):

        heading = self.get_heading(p1, p2)
        steering = heading - r.bearing
        # print "p1:", p1, "p2:", p2,"r.bear ", r.bearing, "steer: ", steering
        return steering
    
    
    
    def moves_plan(self, warehouse, directions, delta_name, moves_costs, start_point, end_point, packet):
        x0 = start_point[0]
        y0 = start_point[1]
        
        h = [[99 for x in range(len(warehouse[0]))] for y in range(len(warehouse))]
        policy = [[' ' for row in range(len(warehouse[0]))] for col in range(len(warehouse))]
        change = True
        while change:
            change = False
            for x in range(len(warehouse)):
                for y in range(len(warehouse[0])):
                    if y == end_point[1] and x == end_point[0] and h[x][y] == 99:
                        if h[x][y] > 0:
                            h[x][y] = 0
                            change = True
                            policy[x][y] = '*'
                                                
                    #if warehouse[x][y] == '.' or (x == x0 and y == y0):
                    if (warehouse[x][y] != '#' and warehouse[x][y] != '@') or (x == x0 and y == y0):
                        for dir in range(len(directions)):
                            x1 = x + directions[dir][0]
                            y1 = y + directions[dir][1]
                            
                            if x1 >= 0 and x1 < len(warehouse) and y1 >= 0 and y1 < len(warehouse[0]):
                                #print 'len warehouse =' + str(len(warehouse[0])) + ',' + str(len(warehouse))
                                #print 'old coordinates = ' + str(x) + ',' + str(y)
                                #print '(x1, y1) = ' + str(x1) + ',' + str(y1)
                                #print 'y1 = ' + str(y1)
                                c1 = h[x1][y1] + moves_costs[dir]
                                
                                if c1 < h[x][y]:
                                    #print 'done'
                                    h[x][y] = c1
                                    change = True
                                    policy[x][y] = delta_name[dir]
        
        #print h
        return policy
    
    
    
    def process_moves(self, init, moves):

        # start from init
        clearance = 0.49
        current_pos = init
        robot = Robot(x=init[0], y=init[1], max_distance=self.max_distance, max_steering=self.max_steering)
        registered_moves = []
        num_boxes_delivered = 0
        unreg_moves = []
        for i in range(len(moves)):
            

            if moves[i][0] == 'lift':
                #lift the box and drop
                prev_pos = current_pos
                changed_dir = 0
                for ind in range(len(self.todo)):
                    if ind == int(moves[i][1]):

                       
                        if current_pos[0] == self.todo[ind][0] or current_pos[1] == self.todo[ind][1]:
                            pass
                        else:
                            robot, registered_moves, unreg_moves = self.approach_func(current_pos, self.todo[ind], robot, registered_moves, unreg_moves)
                            changed_dir = 1
                            current_pos = (robot.x, robot.y)
                        if self.get_distance(current_pos, self.todo[ind]) >= 0.5:
                            previous_state = current_pos
                            dist = self.get_distance(current_pos, self.todo[ind]) - clearance
                            robot, registered_moves, unreg_moves = self.motion(robot, self.truncate_angle(self.get_direction(robot, current_pos, self.todo[ind])), dist, registered_moves, unreg_moves)
                            unreg_moves.append(("lift", self.todo[ind]))
                            registered_moves.append("lift " + moves[i][1])
                            current_pos = (robot.x, robot.y)
                            
                            if changed_dir and self.is_similar_pos(current_pos, previous_state) == False:
                                robot, registered_moves, unreg_moves = self.motion(robot, self.truncate_angle(self.get_direction(robot, current_pos, previous_state)), dist, registered_moves, unreg_moves)
                                current_pos = (robot.x, robot.y)
                            dist = self.get_distance(current_pos, prev_pos)
                            robot, registered_moves, unreg_moves = self.motion(robot, self.truncate_angle(self.get_direction(robot, current_pos, prev_pos)), dist, registered_moves, unreg_moves)
                            current_pos = prev_pos
                            break
            
            elif moves[i][0] == 'move':
                #nomral movement insdie the warehouse
                
                               
                robot, registered_moves, unreg_moves = self.motion(robot, self.truncate_angle(self.get_direction(robot, current_pos, moves[i][1])), self.get_distance(current_pos, moves[i][1]), registered_moves, unreg_moves)
                
                current_pos = moves[i][1]
                
            
            else:
                #drop condition

                prev_pos = current_pos
                changed_dir = 0
                if current_pos[0] == moves[i][1][0] or current_pos[1] == moves[i][1][1]:
                    pass
                else:
                    robot, registered_moves, unreg_moves = self.approach_func(current_pos, moves[i][1], robot, registered_moves, unreg_moves)
                    current_pos = (robot.x, robot.y)
                    changed_dir = 1

                
                if self.get_distance(current_pos, moves[i][1]) > 0.5:
                    previous_state = current_pos
                    dist = self.get_distance(current_pos, moves[i][1]) - clearance
                    robot, registered_moves, unreg_moves = self.motion(robot, self.truncate_angle(self.get_direction(robot, current_pos, moves[i][1])), dist, registered_moves, unreg_moves)
                    current_pos = (robot.x, robot.y)
                    
                    registered_moves.append("down " + str(moves[i][1][0]) + " " + str(moves[i][1][1]))
                    unreg_moves.append(("down", moves[i][1]))
                    if changed_dir and self.is_similar_pos(current_pos, previous_state) == False:
                        robot, registered_moves, unreg_moves = self.motion(robot, self.truncate_angle(self.get_direction(robot, current_pos, previous_state)), self.get_distance(current_pos, previous_state), registered_moves, unreg_moves)
                        current_pos = (robot.x, robot.y)
                else:
                    
                    registered_moves.append("down " + str(moves[i][1][0]) + " " + str(moves[i][1][1]))
                    unreg_moves.append(("down", moves[i][1]))
                num_boxes_delivered += 1

                if num_boxes_delivered < len(self.todo):
                    
                    dist = self.get_distance(current_pos, prev_pos)
                    robot, registered_moves, unreg_moves = self.motion(robot, self.truncate_angle(self.get_direction(robot, current_pos, prev_pos)), dist, registered_moves, unreg_moves)
                    current_pos = prev_pos
                else:
                    pass

        return registered_moves, unreg_moves

    def get_distance(self, a, b):

        dx = a[0] - b[0]
        dy = a[1] - b[1]
        dist = math.sqrt(dx ** 2 + dy ** 2)
        return self.round_float(dist, 2)


    def round_float(self, x, a):
        rnd = round(x, a)
        return float(str(round(float(str(rnd)), a)))

    def truncate_angle(self, t):
        PI = math.pi
        return ((t + PI) % (2 * PI)) - PI

    def motion(self, robot, steer, distance, registered_moves, unreg_moves):
        steer, registered_moves, unreg_moves = self.steer_movement(robot, steer, unreg_moves, registered_moves)
        steer, distance, registered_moves, unreg_moves = self.forward_motion(robot, distance, steer, unreg_moves, registered_moves)
        robot.move(steer, distance)
        unreg_moves.append(("move", (robot.x, robot.y)))
        registered_moves.append("move " + str(steer) + " " + str(distance))
        

        return robot, registered_moves, unreg_moves
    
    def forward_motion(self, robot, distance, steer, unreg_moves, registered_moves):
        max_distance = self.max_distance
        while distance > max_distance:
            robot.move(steer, max_distance)
            unreg_moves.append(("move", (robot.x, robot.y)))
            registered_moves.append("move " + str(steer) + " " + str(max_distance))
            steer = 0.0
            distance = distance - max_distance
            
        return steer, distance, registered_moves, unreg_moves
    
    def minimize_steps1(self, action_list, list_xy):
         
         i = 0
         min_action_list = []
         while i < len(action_list):
             type = action_list[i].split()[0]
             if type == 'move':
                 break
             elif type == 'lift':
                 box_id = action_list[i].split()[1]
                 self._lift_box(box_id)
                 min_action_list.append(action_list[i])
                 i = i + 1
             else:
                 x = action_list[i].split()[1]
                 y = action_list[i].split()[2]
                 # print "Dropping at",x,y
                 min_action_list.append(action_list[i])
                 self._down_box((float(x), float(y)))
                 i = i + 1
 
         robot_pos = -1
         while i < len(action_list):
             # print "i", i, "action", action_list[i]
             fwd = True
 
             type = action_list[i].split()[0]
             # if type == "move":
             #     last_steer, last_dist = self.get_move_action(action_list[i])
             #     # prev_action_ind = i
             # print "Type: ", type
             while fwd == True and i < len(action_list):
                 type = action_list[i].split()[0]
 
                 if type == 'move':
                     distance = action_list[i].split()[2]
                     if distance == '0.0': #distance is 0
                         dir = action_list[i].split()[1]
                         self.robot, min_action_list = self.motion(self.robot, float(dir), 0.0, min_action_list)
                         robot_pos = i
                         prev_action_ind = i
                         i = i+1
                         continue
 
                     dist, steer = self.robot.measure_distance_and_bearing_to(list_xy[i][1])
                     # print "Checking from pos: ", (self.robot.x, self.robot.y), " with dist: ", dist, "steer:", steer, "to: ", list_xy[i][1]
                     if self.is_legal_move(float(steer), float(dist)):
                         # print "legal move to ", i
                         prev_action_ind = i
                         last_steer = steer
                         last_dist = dist
                         # print "jumping to: ", i, list_xy[i]
                         i = i + 1
                     else:
                         # print "moving to i",i, "to", list_xy[i]
                         if prev_action_ind == robot_pos:
                             i = robot_pos+1
                             prev_action_ind = i
                         # print "Moving robot from ", (self.robot.x, self.robot.y), "to", list_xy[prev_action_ind], " index:", prev_action_ind
                         last_dist, last_steer = self.robot.measure_distance_and_bearing_to(list_xy[prev_action_ind][1])
                         robot_pos = prev_action_ind
                         # prev_action_ind = i
                         self.robot, min_action_list = self.motion(self.robot, last_steer, last_dist,min_action_list)
 
                         # print "New robot pos: ", self.robot.x, self.robot.y
 
                         # min_action_list.append("move " + str(last_steer) + " " + str(last_dist))
                         fwd = False
                         # i = i + 1
                 else:
                     fwd = False
 
                     if robot_pos != prev_action_ind:
                         # print "Moving robot from ", (self.robot.x, self.robot.y), "to", list_xy[prev_action_ind], " index:", prev_action_ind
                         last_dist, last_steer = self.robot.measure_distance_and_bearing_to(list_xy[prev_action_ind][1])
                         # prev_action_ind = i
                         # self.robot.move(last_steer, last_dist)
                         self.robot, min_action_list = self.motion(self.robot, last_steer, last_dist, min_action_list)
                         robot_pos = prev_action_ind
                         # print "New robot pos: ", self.robot.x, self.robot.y
 
                         # min_action_list.append("move " + str(last_steer) + " " + str(last_dist))
 
                     min_action_list.append(action_list[i])
                     action = action_list[i].split()
                     if type == 'lift':
                         box_id = action[1]
                         self._lift_box(box_id)
                     else:
                         x = action[1]
                         y = action[2]
                         # print "Dropping at", x, y
                         self._down_box((float(x), float(y)))
                     i = i + 1
                     # prev_action_ind = i
                     # robot_pos = i
                     # i = i + 1
 
 
         return min_action_list
    
    def steer_movement(self, robot, steer, unreg_moves, registered_moves):
        min_steer = self.min_steering
        max_steer = self.max_steering
        while steer < min_steer:
            distance_moved = 0
            robot.move(min_steer, distance_moved)
            unreg_moves.append(("move", (robot.x, robot.y)))
            registered_moves.append("move " + str(min_steer) + " 0.0")
            steer = steer - min_steer
        
        while steer > max_steer:
            robot.move(max_steer, 0.0)
            unreg_moves.append(("move", (robot.x, robot.y)))
            registered_moves.append("move " + str(max_steer) + " 0.0")
            steer = steer - max_steer
        
        return steer, registered_moves, unreg_moves
    
    
    
    def approach_func(self, a, b, robot, registered_moves, unreg_moves):

        (x1, y1) = a
        (x2, y2) = b
        (x, y) = (x2, y1)
        
        (a1, b1) = self.grid_xy(x, y)
        (a2, b2) = self.grid_xy(x - 0.25, y)
        (a3, b3) = self.grid_xy(x + 0.25, y)
        
        flag = 0
        if self.discrete_warehouse[a1][b1] != '#' and self.discrete_warehouse[a2][b2] != '#' and self.discrete_warehouse[a3][b3] != '#' and (a1, b1) not in self.classic_todo and (a2, b2) not in self.classic_todo and (a3, b3) not in self.classic_todo:
            flag = 1

        if flag == 0:
            distance1 = self.get_distance(a, (x1, y2))
            turn = self.truncate_angle(self.get_direction(robot, a, (x1, y2)))
            robot.move(turn, distance1)
            registered_moves.append("move " + str(turn) + " " + str(distance1))
            unreg_moves.append(("move", (robot.x, robot.y)))
        else:
            distance1 = self.get_distance(a, (x, y))
            turn = self.truncate_angle(self.get_direction(robot, a, (x, y)))
            robot.move(turn, distance1)
            registered_moves.append("move " + str(turn) + " " + str(distance1))
            unreg_moves.append(("move", (robot.x, robot.y)))
        
        return robot, registered_moves, unreg_moves
    
    def grid_xy(self, x, y):
        return ((int(y * -1), int(x)))

    
class State:

    """
    This code has been adopted from testing_suite_partB.py 
    """
    MOVE_COST = 1.0
    BOX_LIFT_COST = 2.0
    BOX_DOWN_COST = 1.5
    ILLEGAL_MOVE_PENALTY = 100.
    BOX_SIZE = 0.1  # 1/2 of box width and height
    BOX_DIAG = 0.1414
    ROBOT_RADIUS = 0.25
    OBSTACLE_DIAG = 1.414
    ROBOT_STEP_SIZE = 0.1

    def __init__(self, warehouse, todo, max_distance, max_steering, init):
        self.boxes_delivered = []
        self.total_cost = 0
        self.max_distance = max_distance
        self.min_steering = -1.0 * max_steering
        self.max_steering = max_steering
        self._set_initial_state_from(warehouse, todo)
        self.robot_is_crashed = False
        self.init = init

    def _set_initial_state_from(self, warehouse, todo):

        rows = len(warehouse)
        cols = len(warehouse[0])

        self.dropzone = dict()
        self.boxes = dict()
        self.obstacles = []

        # set the warehouse limits:  min_x = 0.0,  max_y = 0.0
        self.warehouse_limits = {'max_x': float(cols), 'min_y': float(-rows)}

        self.warehouse_limits['segments'] = []
        # West segnent (x0,y0) -> (x1,y1)
        self.warehouse_limits['segments'].append(
            [(self.ROBOT_RADIUS, 0.0),
             (self.ROBOT_RADIUS, self.warehouse_limits['min_y'])])
        # South segment
        self.warehouse_limits['segments'].append(
            [(0.0, self.warehouse_limits['min_y'] + self.ROBOT_RADIUS),
             (self.warehouse_limits['max_x'], self.warehouse_limits['min_y'] + self.ROBOT_RADIUS)])
        # East segment
        self.warehouse_limits['segments'].append(
            [(self.warehouse_limits['max_x'] - self.ROBOT_RADIUS, self.warehouse_limits['min_y']),
             (self.warehouse_limits['max_x'] - self.ROBOT_RADIUS, 0.0)])
        # North segment
        self.warehouse_limits['segments'].append(
            [(self.warehouse_limits['max_x'], -self.ROBOT_RADIUS),
             (0.0, -self.ROBOT_RADIUS)])

        for i in range(rows):
            for j in range(cols):
                this_square = warehouse[i][j]
                x, y = float(j), -float(i)

                # set the obstacle limits, centers, and edges compensated for robot radius
                # precompute these values to save time later
                if this_square == '#':
                    obstacle = dict()

                    # obstacle edges
                    obstacle['min_x'] = x
                    obstacle['max_x'] = x + 1.0
                    obstacle['min_y'] = y - 1.0
                    obstacle['max_y'] = y

                    # center of obstacle
                    obstacle['ctr_x'] = x + 0.5
                    obstacle['ctr_y'] = y - 0.5

                    # compute clearance parameters for robot
                    obstacle = self._dilate_obstacle_for_robot(obstacle)

                    self.obstacles.append(obstacle)

                    # set the dropzone limits
                elif this_square == '@':
                    self.dropzone['min_x'] = x
                    self.dropzone['max_x'] = x + 1.0
                    self.dropzone['min_y'] = y - 1.0
                    self.dropzone['max_y'] = y
                    self.dropzone['ctr_x'] = x + 0.5
                    self.dropzone['ctr_y'] = y - 0.5

        for i in range(len(todo)):
            # set up the parameters for processing the box as an obstacle and for
            # picking it up and setting it down
            box = dict()
            # box edges
            box['min_x'] = todo[i][0] - self.BOX_SIZE
            box['max_x'] = todo[i][0] + self.BOX_SIZE
            box['min_y'] = todo[i][1] - self.BOX_SIZE
            box['max_y'] = todo[i][1] + self.BOX_SIZE

            # center of obstacle
            box['ctr_x'] = todo[i][0]
            box['ctr_y'] = todo[i][1]

            # compute clearance parameters for robot
            box = self._dilate_obstacle_for_robot(box)

            self.boxes[str(i)] = box

        # initialize the robot in the center of the dropzone at a bearing pointing due east
        self.robot = Robot(x=self.dropzone['ctr_x'], y=self.dropzone['ctr_y'], bearing=0.0,
                           max_distance=self.max_distance, max_steering=self.max_steering)

        self.box_held = None

    def get_move_action(self, action_str):
        action = action_str.split()
        # print "Action: ", action
        action_type = action[0]
        steer = action[1]
        dist = action[2]
        return steer, dist

    def is_legal_move(self, steering, distance):
        distance_ok = 0.0 <= distance <= self.max_distance
        steering_ok = (-self.max_steering) <= steering <= self.max_steering
        # distance_ok = True
        # steering_ok = True
        path_is_traversable = True

        destination = self.robot.find_next_point(steering, distance)
        if distance > 0.0:
            path_is_traversable, clear_distance = self._is_traversable(destination, distance, steering)

        is_legal_move = distance_ok and steering_ok and path_is_traversable

        return is_legal_move

    def motion(self, robot, steer, distance, min_moves):

        # handle steer
        while steer > self.max_steering:
            robot.move(self.max_steering, 0.0)
            min_moves.append("move " + str(self.max_steering) + " 0.0")

            steer = steer - self.max_steering

        while steer < self.min_steering:
            robot.move(self.min_steering, 0.0)
            min_moves.append("move " + str(self.min_steering) + " 0.0")

            steer = steer - self.min_steering

        # handle distance
        while distance > self.max_distance:
            robot.move(steer, self.max_distance)
            min_moves.append("move " + str(steer) + " " + str(self.max_distance))

            steer = 0.0
            distance = distance - self.max_distance
        robot.move(steer, distance)
        min_moves.append("move " + str(steer) + " " + str(distance))

        return robot, min_moves

    def minimize_steps(self, action_list, list_xy):
        # print "########################################################"
        # print "lengths: action_list ", len(action_list), "list_xy", len(list_xy)
        #
        # print "actions list", action_list
        #
        # print "robot pos", self.robot.x, self.robot.y
        i = 0
        min_action_list = []
        while i < len(action_list):
            type = action_list[i].split()[0]
            if type == 'move':
                break
            elif type == 'lift':
                box_id = action_list[i].split()[1]
                self._lift_box(box_id)
                min_action_list.append(action_list[i])
                i = i + 1
            else:
                x = action_list[i].split()[1]
                y = action_list[i].split()[2]
                # print "Dropping at",x,y
                min_action_list.append(action_list[i])
                self._down_box((float(x), float(y)))
                i = i + 1

        robot_pos = -1
        while i < len(action_list):
            # print "i", i, "action", action_list[i]
            fwd = True

            type = action_list[i].split()[0]
            # if type == "move":
            #     last_steer, last_dist = self.get_move_action(action_list[i])
            #     # prev_action_ind = i
            # print "Type: ", type
            while fwd == True and i < len(action_list):
                type = action_list[i].split()[0]

                if type == 'move':
                    distance = action_list[i].split()[2]
                    if distance == '0.0': #distance is 0
                        dir = action_list[i].split()[1]
                        self.robot, min_action_list = self.motion(self.robot, float(dir), 0.0, min_action_list)
                        robot_pos = i
                        prev_action_ind = i
                        i = i+1
                        continue

                    dist, steer = self.robot.measure_distance_and_bearing_to(list_xy[i][1])
                    # print "Checking from pos: ", (self.robot.x, self.robot.y), " with dist: ", dist, "steer:", steer, "to: ", list_xy[i][1]
                    if self.is_legal_move(float(steer), float(dist)):
                        # print "legal move to ", i
                        prev_action_ind = i
                        last_steer = steer
                        last_dist = dist
                        # print "jumping to: ", i, list_xy[i]
                        i = i + 1
                    else:
                        # print "moving to i",i, "to", list_xy[i]
                        if prev_action_ind == robot_pos:
                            i = robot_pos+1
                            prev_action_ind = i
                        # print "Moving robot from ", (self.robot.x, self.robot.y), "to", list_xy[prev_action_ind], " index:", prev_action_ind
                        last_dist, last_steer = self.robot.measure_distance_and_bearing_to(list_xy[prev_action_ind][1])
                        robot_pos = prev_action_ind
                        # prev_action_ind = i
                        self.robot, min_action_list = self.motion(self.robot, last_steer, last_dist,min_action_list)

                        # print "New robot pos: ", self.robot.x, self.robot.y

                        # min_action_list.append("move " + str(last_steer) + " " + str(last_dist))
                        fwd = False
                        # i = i + 1
                else:
                    fwd = False

                    if robot_pos != prev_action_ind:
                        # print "Moving robot from ", (self.robot.x, self.robot.y), "to", list_xy[prev_action_ind], " index:", prev_action_ind
                        last_dist, last_steer = self.robot.measure_distance_and_bearing_to(list_xy[prev_action_ind][1])
                        # prev_action_ind = i
                        # self.robot.move(last_steer, last_dist)
                        self.robot, min_action_list = self.motion(self.robot, last_steer, last_dist, min_action_list)
                        robot_pos = prev_action_ind
                        # print "New robot pos: ", self.robot.x, self.robot.y

                        # min_action_list.append("move " + str(last_steer) + " " + str(last_dist))

                    min_action_list.append(action_list[i])
                    action = action_list[i].split()
                    if type == 'lift':
                        box_id = action[1]
                        self._lift_box(box_id)
                    else:
                        x = action[1]
                        y = action[2]
                        # print "Dropping at", x, y
                        self._down_box((float(x), float(y)))
                    i = i + 1
                    # prev_action_ind = i
                    # robot_pos = i
                    # i = i + 1


        return min_action_list

    # Update the system state according to the specified action
    def update_according_to(self, action):

        # what type of move is it?
        action = action.split()
        action_type = action[0]

        if action_type == 'move':
            steering, distance = action[1:]
            self._attempt_move(float(steering), float(distance))

        elif action_type == 'lift':
            box = action[1]
            self._attempt_lift(box)

        elif action_type == 'down':
            x, y = action[1:]
            self._attempt_down(float(x), float(y))

        else:
            # improper move format: kill test
            raise Exception('improperly formatted action: {}'.format(''.join(action)))

    def _attempt_move(self, steering, distance):
        # - The robot may move between 0 and max_distance
        # - The robot may turn between -max_steering and +max_steering
        # - The warehouse does not "wrap" around.
        # Costs
        # - 1+ distance traversed on that turn
        # - Crash: 100*distance attempted.

        # Illegal moves - the robot will not move, but the standard cost will be incurred.
        # - Moving a distance outside of [0,max_distance]
        # - Steering angle outside [-max_steering, max_steering]

        try:
            distance_ok = 0.0 <= distance <= self.max_distance
            steering_ok = (-self.max_steering) <= steering <= self.max_steering
            path_is_traversable = True

            destination = self.robot.find_next_point(steering, distance)
            if distance > 0.0:
                path_is_traversable, clear_distance = self._is_traversable(destination, distance, steering)

            is_legal_move = distance_ok and steering_ok and path_is_traversable
            if is_legal_move:
                self.robot.move(steering, distance)
                self._increase_total_cost_by(self.MOVE_COST + distance)
            elif not path_is_traversable:
                self._increase_total_cost_by(self.ILLEGAL_MOVE_PENALTY * distance)
                self.robot.move(steering, clear_distance)

        except ValueError:
            raise Exception('improperly formatted move destination: {} {}'.format(row, col))

    def _attempt_lift(self, box_id):
        # - The robot may pick up a box that is within a distance of 0.5 of the robot's center.
        # - The cost to pick up a box is 2, regardless of the direction the box is relative to the robot.
        # - While holding a box, the robot may not pick up another box.
        # Illegal lifts (do not lift a box but incur cost of lift):
        # - picking up a box that doesn't exist or is too far away
        # - picking up a box while already holding one

        try:
            self._increase_total_cost_by(self.BOX_LIFT_COST)
            box_position = (self.boxes[box_id]['ctr_x'], self.boxes[box_id]['ctr_y'])

            box_is_adjacent = compute_distance((self.robot.x, self.robot.y), box_position) <= 0.5
            robot_has_box = self._robot_has_box()

            is_legal_lift = box_is_adjacent and (not robot_has_box)
            if is_legal_lift:
                self._lift_box(box_id)
            else:
                # print "*** Could not lift box: box_is_adjacent = {}, robot_has_box = {} ".format(box_is_adjacent,robot_has_box)
                pass

        except KeyError:
            raise Exception('improper key {}'.format(box_id))

    def _attempt_down(self, x, y):
        # - The robot may put a box down within a distance of 0.5 of the robot's center.
        #   The cost to set a box down is 1.5 (regardless of the direction in which the robot puts down the box).
        # - If a box is placed on the '@' space, it is considered delivered and is removed from the ware-
        #   house.
        # Illegal moves (do not set box down but incur cost):
        # - putting down a box too far away or so that it's touching a wall, the warehouse exterior,
        #   another box, or the robot
        # - putting down a box while not holding a box

        try:

            self._increase_total_cost_by(self.BOX_DOWN_COST)
            destination = (x, y)

            destination_is_adjacent = compute_distance((self.robot.x, self.robot.y), destination) <= 0.5
            destination_is_open = self._is_open_for_box(destination)
            destination_is_within_warehouse = self._is_box_within_warehouse(destination)
            robot_has_box = self._robot_has_box()

            is_legal_down = (destination_is_adjacent and destination_is_open
                             and destination_is_within_warehouse and robot_has_box)

            if is_legal_down:
                self._down_box(destination)

        except ValueError:
            raise Exception('improperly formatted down destination: {} {}'.format(row, col))

    # Increment the total cost
    def _increase_total_cost_by(self, amount):
        self.total_cost += amount

    # Assumes the warehouse NW corner is (0,0) and SE corner is (max_x, min_y)
    def _is_box_within_warehouse(self, coordinates):
        x, y = coordinates
        return self.BOX_SIZE < x < (self.warehouse_limits['max_x'] - self.BOX_SIZE) and \
               (self.warehouse_limits['min_y'] + self.BOX_SIZE) < y < -self.BOX_SIZE

        # Check if the box within the dropzone

    def _is_box_within_dropzone(self, coordinates):
        x, y = coordinates
        return (self.dropzone['min_x'] + self.BOX_SIZE) < x < (self.dropzone['max_x'] - self.BOX_SIZE) and \
               (self.dropzone['min_y'] + self.BOX_SIZE) < y < (self.dropzone['max_y'] - self.BOX_SIZE)

        # Verify a box can be placed at the desired coordinates

    def _is_open_for_box(self, coordinates):

        # process all boxes still remaining
        for b in self.boxes:
            if not self._is_box_outside_obstacle(coordinates, self.boxes[b]):
                # print '*** Could not set down - box {} in the way ***'.format(b)
                return False

        # process all obstacles
        for o in self.obstacles:
            if not self._is_box_outside_obstacle(coordinates, o):
                # print '*** Could not set down - obstacle {} in the way ***'.format(0)
                return False

        return True

    # Check the box to make sure it is outside an obstacle (or other box)
    def _is_box_outside_obstacle(self, coordinates, obstacle):

        # the center coordinates of the box to be placed
        x, y = coordinates

        if (x - self.BOX_SIZE) >= obstacle['max_x']:
            return True
        elif (x + self.BOX_SIZE) <= obstacle['min_x']:
            return True
        elif (y - self.BOX_SIZE) >= obstacle['max_y']:
            return True
        elif (y + self.BOX_SIZE) <= obstacle['min_y']:
            return True
        else:
            return False

    # Check the path to make sure the robot can traverse it without running into
    # boxes, obstacles, or the warehouse walls
    def _is_traversable(self, destination, distance, steering):

        NUDGE_DISTANCE = 0.01

        # end points of trajectory
        t1 = destination
        if not self.robot_is_crashed:
            t0 = (self.robot.x, self.robot.y)
            # the distance to check against
            chk_distance = distance

        else:
            # in the event the robot is crashed don't use the current robot point to evaluate
            # collisions because by definition it is colliding.  Define the trajectory line
            # segment start point just a bit farther out to see if a new collision will occur.
            t0 = self.robot.find_next_point(steering, NUDGE_DISTANCE)
            chk_distance = distance - NUDGE_DISTANCE

        # Is the path too close to any box
        min_distance_to_intercept = chk_distance
        self.robot_is_crashed = False
        for box_id, box in self.boxes.iteritems():
            # do a coarse check to see if the center of the obstacle
            # is too far away to be concerned with
            dst = self._distance_point_to_line_segment((box['ctr_x'], box['ctr_y']), t0, t1)
            if (dst <= self.BOX_DIAG + self.ROBOT_RADIUS):
                # refine the intercept computation
                dst, intercept_point = self._check_intersection(t0, t1, box)
                if dst < min_distance_to_intercept:
                    min_distance_to_intercept = dst
                    min_intercept_point = intercept_point

        # Is the path too close to any obstacle
        for o in self.obstacles:
            # do a coarse check to see if the center of the obstacle
            # is too far away to be concerned with
            dst = self._distance_point_to_line_segment((o['ctr_x'], o['ctr_y']), t0, t1)
            if (dst <= self.OBSTACLE_DIAG + self.ROBOT_RADIUS):
                # refine the intercept computation
                dst, intercept_point = self._check_intersection(t0, t1, o)
                if dst < min_distance_to_intercept:
                    min_distance_to_intercept = dst
                    min_intercept_point = intercept_point

        # Check the edges of the warehouse
        dst, intercept_point = self._check_intersection(t0, t1, self.warehouse_limits)
        if dst < min_distance_to_intercept:
            min_distance_to_intercept = dst
            min_intercept_point = intercept_point

        if min_distance_to_intercept < chk_distance:
            self.robot_is_crashed = True
            # print "*** Robot crashed at {p[0]:6.2f} {p[1]:6.2f} ***".format(p=min_intercept_point)
            return False, compute_distance((self.robot.x, self.robot.y), min_intercept_point)

        return True, distance


        # Check if the trajectory intersects with a square obstacle

    def _check_intersection(self, t0, t1, obstacle):

        min_distance_to_intercept = 1.e6
        min_intercept_point = (0., 0.)

        # check each segment
        for s in obstacle['segments']:
            dst, intercept_point = self._linesegment_intersection(t0, t1, s[0], s[1])
            if dst < min_distance_to_intercept:
                min_distance_to_intercept = dst
                min_intercept_point = intercept_point

                # if circular corners are defined - check them
        # circular corners occur when dilating a rectangle with a circle
        if obstacle.has_key('corners'):
            for c in obstacle['corners']:
                dst, intercept_point = self._corner_intersection(t0, t1, c)
                if dst < min_distance_to_intercept:
                    min_distance_to_intercept = dst
                    min_intercept_point = intercept_point

        return min_distance_to_intercept, min_intercept_point

    # Find the intersection of a line segment and a semicircle as defined in
    # the corner dictionary
    # Use quadratic solution to solve simultaneous equations for
    # (x-a)^2 + (y-b)^2 = r^2 and y = mx + c
    def _corner_intersection(self, t0, t1, corner):

        dst = 1.e6
        intercept_point = (0., 0.)

        # Note:  changing nomenclature here so that circle center is a,b
        # and line intercept is c (not b as above)
        a = corner['ctr_x']  # circle ctrs
        b = corner['ctr_y']
        r = corner['radius']

        # check the case for infinite slope
        dx = t1[0] - t0[0]

        # Find intersection assuming vertical trajectory
        if abs(dx) < 1.e-6:
            x0 = t0[0] - a
            # qa = 1.
            qb = -2. * b
            qc = b * b + x0 * x0 - r * r
            disc = qb * qb - 4. * qc

            if disc >= 0.:
                sd = math.sqrt(disc)
                xp = xm = t0[0]
                yp = (-qb + sd) / 2.
                ym = (-qb - sd) / 2.

        # Find intersection assuming non vertical trajectory
        else:
            m = (t1[1] - t0[1]) / dx  # slope of line
            c = t0[1] - m * t0[0]  # y intercept of line

            qa = 1. + m * m
            qb = 2. * (m * c - m * b - a)
            qc = a * a + b * b + c * c - 2. * b * c - r * r

            disc = qb * qb - 4. * qa * qc

            if disc >= 0.:
                sd = math.sqrt(disc)
                xp = (-qb + sd) / (2. * qa)
                yp = m * xp + c
                xm = (-qb - sd) / (2. * qa)
                ym = m * xm + c

        if disc >= 0.:
            dp2 = dm2 = 1.e6
            if corner['min_x'] <= xp <= corner['max_x'] and corner['min_y'] <= yp <= corner['max_y']:
                dp2 = (xp - t0[0]) ** 2 + (yp - t0[1]) ** 2

            if corner['min_x'] <= xm <= corner['max_x'] and corner['min_y'] <= ym <= corner['max_y']:
                dm2 = (xm - t0[0]) ** 2 + (ym - t0[1]) ** 2

            if dp2 < dm2:
                # make sure the intersection point is actually on the trajectory segment
                if self._distance_point_to_line_segment((xp, yp), t0, t1) < 1.e-6:
                    dst = math.sqrt(dp2)
                    intercept_point = (xp, yp)
            else:
                if self._distance_point_to_line_segment((xm, ym), t0, t1) < 1.e-6:
                    dst = math.sqrt(dm2)
                    intercept_point = (xm, ym)

        return dst, intercept_point

    # Find the distance from a point to a line segment
    # This function is used primarily to find the distance between a trajectory
    # segment defined by l0,l1 and the center of an obstacle or box specified
    # by point p.  For a reference see the lecture on SLAM and the segmented CTE
    def _distance_point_to_line_segment(self, p, l0, l1):

        dst = 1.e6
        dx = l1[0] - l0[0]
        dy = l1[1] - l0[1]

        # check that l0,l1 don't describe a point
        d2 = (dx * dx + dy * dy)

        if abs(d2) > 1.e-6:

            t = ((p[0] - l0[0]) * dx + (p[1] - l0[1]) * dy) / d2

            # if point is on line segment
            if 0.0 <= t <= 1.0:
                intx, inty = l0[0] + t * dx, l0[1] + t * dy
                dx, dy = p[0] - intx, p[1] - inty

            # point is beyond end point
            elif t > 1.0:
                dx, dy = p[0] - l1[0], p[1] - l1[1]

            # point is before beginning point
            else:
                dx, dy = p[0] - l0[0], p[1] - l0[1]

            dst = math.sqrt(dx * dx + dy * dy)

        else:
            dx, dy = p[0] - l0[0], p[1] - l0[1]
            dst = math.sqrt(dx * dx + dy * dy)

        return dst

    # Check for the intersection of two line segments.  This function assumes that the robot
    # trajectory starts at p0 and ends at p1.  The segment being checked is q0 and q1
    # There is no check for colinearity because the obstacle is assumed to have orthogonal
    # sides and so the intersection for a trajectory that is colinear and overlapping with
    # one side will intersect with the orthogonal side, since by definition the starting
    # point of the trajectory is always outside the obstacle.
    # For a reference see:
    #  http://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect/565282#565282
    def _linesegment_intersection(self, p0, p1, q0, q1):

        eps = 1.0e-6
        dst = 1.0e6
        intersection = (0., 0.)

        r = p1[0] - p0[0], p1[1] - p0[1]
        s = q1[0] - q0[0], q1[1] - q0[1]
        qmp = q0[0] - p0[0], q0[1] - p0[1]

        rxs = r[0] * s[1] - r[1] * s[0]
        qmpxr = qmp[0] * r[1] - qmp[1] * r[0]

        if abs(rxs) >= eps:

            # check for intersection
            # parametric equations for intersection
            # t = (q - p) x s / (r x s)
            t = (qmp[0] * s[1] - qmp[1] * s[0]) / rxs

            # u = (q - p) x r / (r x s)
            u = qmpxr / rxs

            # Note that u and v can be slightly out of this range due to
            # precision issues so we round them
            if (0.0 <= np.round(t, 4) <= 1.0) and (0.0 <= np.round(u, 4) <= 1.0):
                dx, dy = t * r[0], t * r[1]
                dst = math.sqrt(dx * dx + dy * dy)
                intersection = (p0[0] + dx, p0[1] + dy)

        return dst, intersection

    # Check whether or not robot is holding a box
    def _robot_has_box(self):
        return self.box_held != None

    # Lift the box
    def _lift_box(self, box_id):

        self.boxes.pop(box_id)
        self.box_held = box_id

    # Set the box down at the specified destination and recompute its parameters
    # for obstacle avoidance computation
    def _down_box(self, destination):

        # - If a box is placed on the '@' space, it is considered delivered and is removed from the ware-
        #   house.
        x, y = destination

        if self._is_box_within_dropzone(destination):
            self._deliver_box(self.box_held)
        else:
            # set up the parameters for processing the box as an obstacle and for
            # picking it up and setting it down later
            box = dict()
            # box edges
            box['min_x'] = x - self.BOX_SIZE
            box['max_x'] = x + self.BOX_SIZE
            box['min_y'] = y - self.BOX_SIZE
            box['max_y'] = y + self.BOX_SIZE

            # center of obstacle
            box['ctr_x'] = x
            box['ctr_y'] = y

            # compute clearance parameters for robot
            box = self._dilate_obstacle_for_robot(box)

            self.boxes[self.box_held] = box

        self.box_held = None

    # Deliver the box by appending to the list of delivered boxes
    def _deliver_box(self, box_id):
        self.boxes_delivered.append(box_id)

    # Compute the clearance parameters for the robot by dilating the object using
    # a circle the radius of the robot.  The resulting shape is rectangular with
    # rounded corners
    def _dilate_obstacle_for_robot(self, obstacle):

        # line segments dilated for robot intersection
        obstacle['segments'] = []
        # West segnent
        obstacle['segments'].append([(obstacle['min_x'] - self.ROBOT_RADIUS, obstacle['max_y']), \
                                     (obstacle['min_x'] - self.ROBOT_RADIUS, obstacle['min_y'])])
        # South segment
        obstacle['segments'].append([(obstacle['min_x'], obstacle['min_y'] - self.ROBOT_RADIUS), \
                                     (obstacle['max_x'], obstacle['min_y'] - self.ROBOT_RADIUS)])
        # East segment
        obstacle['segments'].append([(obstacle['max_x'] + self.ROBOT_RADIUS, obstacle['min_y']), \
                                     (obstacle['max_x'] + self.ROBOT_RADIUS, obstacle['max_y'])])
        # North segment
        obstacle['segments'].append([(obstacle['max_x'], obstacle['max_y'] + self.ROBOT_RADIUS), \
                                     (obstacle['min_x'], obstacle['max_y'] + self.ROBOT_RADIUS)])

        obstacle['corners'] = []
        # NW corner
        cornerdef = dict()
        cornerdef['ctr_x'] = obstacle['min_x']
        cornerdef['ctr_y'] = obstacle['max_y']
        cornerdef['radius'] = self.ROBOT_RADIUS
        cornerdef['min_x'] = obstacle['min_x'] - self.ROBOT_RADIUS
        cornerdef['max_x'] = obstacle['min_x']
        cornerdef['min_y'] = obstacle['max_y']
        cornerdef['max_y'] = obstacle['max_y'] + self.ROBOT_RADIUS
        obstacle['corners'].append(cornerdef)

        # SW corner
        cornerdef = dict()
        cornerdef['ctr_x'] = obstacle['min_x']
        cornerdef['ctr_y'] = obstacle['min_y']
        cornerdef['radius'] = self.ROBOT_RADIUS
        cornerdef['min_x'] = obstacle['min_x'] - self.ROBOT_RADIUS
        cornerdef['max_x'] = obstacle['min_x']
        cornerdef['min_y'] = obstacle['min_y'] - self.ROBOT_RADIUS
        cornerdef['max_y'] = obstacle['min_y']
        obstacle['corners'].append(cornerdef)

        # SE corner
        cornerdef = dict()
        cornerdef['ctr_x'] = obstacle['max_x']
        cornerdef['ctr_y'] = obstacle['min_y']
        cornerdef['radius'] = self.ROBOT_RADIUS
        cornerdef['min_x'] = obstacle['max_x']
        cornerdef['max_x'] = obstacle['max_x'] + self.ROBOT_RADIUS
        cornerdef['min_y'] = obstacle['min_y'] - self.ROBOT_RADIUS
        cornerdef['max_y'] = obstacle['min_y']
        obstacle['corners'].append(cornerdef)

        # NE corner
        cornerdef = dict()
        cornerdef['ctr_x'] = obstacle['max_x']
        cornerdef['ctr_y'] = obstacle['max_y']
        cornerdef['radius'] = self.ROBOT_RADIUS
        cornerdef['min_x'] = obstacle['max_x']
        cornerdef['max_x'] = obstacle['max_x'] + self.ROBOT_RADIUS
        cornerdef['min_y'] = obstacle['max_y']
        cornerdef['max_y'] = obstacle['max_y'] + self.ROBOT_RADIUS
        obstacle['corners'].append(cornerdef)

        return obstacle

    def get_boxes_delivered(self):
        return self.boxes_delivered

    def get_total_cost(self):
        return self.total_cost

    def print_to_console(self):
        print ''
        print 'Robot state:'
        print '\t x = {:6.2f}, y = {:6.2f}, hdg = {:6.2f}'.format(self.robot.x, self.robot.y,
                                                                  self.robot.bearing * 180. / PI)
        print 'Box state:'
        for box_id, box in self.boxes.iteritems():
            print '\tbox id {}, x = {:6.2f}, y = {:6.2f}'.format(box_id, box['ctr_x'], box['ctr_y'])
        print 'Total cost:', self.total_cost
        print 'Box id held:', self.box_held
        print 'Delivered:', self.boxes_delivered
        print ''   